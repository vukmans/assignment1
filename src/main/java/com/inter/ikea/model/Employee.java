package com.inter.ikea.model;

public class Employee {
    
    public Integer id;
    public Integer salary;

    public Employee(){}

    public Employee setId(Integer id) {
        this.id = id;
        return this;
    }

    public Employee setSalary() {
        this.salary = (int)(Math.random()*(99999-1000+1))+1000;
        return this;
    }

    public Integer getId() {
        return id;
    }

    public Integer getSalary() {
        return salary;
    }

    
}
