package com.inter.ikea;

import java.util.ArrayList;

import com.inter.ikea.model.Employee;
import com.inter.ikea.search.Binary;
import com.inter.ikea.search.Linear;

import lombok.extern.slf4j.Slf4j;




/**
 * 100 employees, meaning id [0,99]
 * promotionList 10 entries [0-199] to generate false positves for search algos
 * 
 */
@Slf4j
public class Main 
{
    public static void main( String[] args )
    {

        ArrayList<Employee> employees= new ArrayList<Employee>();
        ArrayList<Integer> employeesList = new ArrayList<Integer>();
        for(int i=0; i<100; ++i){
            employees.add(new Employee().setId(i).setSalary());
            employeesList.add(i);
        }

        ArrayList<Integer> promotionList = new ArrayList<Integer>();
        for(int i=0; i<10; ++i){
            promotionList.add((int)(Math.random()*200));
        }

        Linear linearSearch = new Linear();
        Binary binarySearch = new Binary();
        ArrayList<Integer> eligibleLinear = linearSearch.search(employeesList, promotionList);
        ArrayList<Integer> eligibleBinary = binarySearch.search(employeesList, promotionList);

        log.info("******** PROMOTIONS BY LINEAR SEARCH ********");
        for(Integer i : eligibleLinear){
            log.info("Employee id: " + i 
            + " Old salary: " + employees.get(i).getSalary());
            log.info("Setting new salary..");
            employees.get(i).setSalary();
            log.info("Employee id: " + i 
            + " New salary: " + employees.get(i).getSalary());
            log.info(" ");
        }

        log.info("******** PROMOTIONS BY BINARY SEARCH ********");
        for(Integer i : eligibleBinary){
            log.info("Employee id: " + i 
            + " Old salary: " + employees.get(i).getSalary());
            log.info("Setting new salary..");
            employees.get(i).setSalary();
            log.info("Employee id: " + i 
            + " New salary: " + employees.get(i).getSalary());
            log.info(" ");
        }

    }
}
