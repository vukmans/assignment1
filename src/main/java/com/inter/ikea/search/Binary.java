package com.inter.ikea.search;

import java.util.ArrayList;

public class Binary implements SearchInterface {

    public Integer binarySearch(ArrayList<Integer> array, Integer lb, Integer hb, Integer input) {
        Integer index = -1;

        while(lb <= hb){
            int mid = (lb+hb)/2;
            if(array.get(mid) < input){
                lb = mid+1;
            } else if (array.get(mid) > input) {
                hb=mid-1;
            } else if (array.get(mid) == input){
                index = mid;
                break;
            }
        }
        return index;
    }

    @Override
    public ArrayList<Integer> search(ArrayList<Integer> array, ArrayList<Integer> input) {
        ArrayList<Integer> result = new ArrayList<Integer>();
        Integer higherBound = array.size()-1;
        Integer lowerBound = 0;
        for(Integer i : input){
            Integer tmp = binarySearch(array, lowerBound, higherBound, i);
            if(!(tmp==-1)) result.add(tmp);
        }

        return result;
    }

}
