package com.inter.ikea.search;

import java.util.ArrayList;

interface SearchInterface {
    ArrayList<Integer> search(ArrayList<Integer> array, ArrayList<Integer> input);
}
